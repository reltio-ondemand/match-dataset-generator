package com.reltio.match.common_asset_generator;

import com.github.javafaker.Faker;
import com.google.api.gax.rpc.ServerStream;
import com.google.cloud.spanner.*;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.rpc.Code;
import com.google.spanner.v1.BatchWriteResponse;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.*;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.channels.Channels;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class DBDataGenApp {
    private final static Logger logger = LoggerFactory.getLogger(DBDataGenApp.class);

    private static final Option OUTPUT_ARG = new Option("output", true, "output type: csv, avro, spanner");
    private static final Option STORAGE_ARG = new Option("storage", true, "storage type: local, gs");
    private static final Option STORAGE_PATH_ARG = new Option("path", true, "storage path");
    private static final Option ENTITY_CNT_ARG = new Option("entityCnt", true, "entity count");
    private static final Option ENTITY_ATTR_CNT_ARG = new Option("attributesCnt", true, "attributes count");
    private static final Option ENTITY_ATTR_VALUES_CNT_ARG = new Option("attributeValuesCnt", true, "attribute values count");
    private final int nThreads = 40;

    private long entityCount = 1_000_000_000;
//    private long entityCount = 1_000_000;
//    private long entityCount = 1_000;

    private int attributesCount = 10;
    private int attributeValuesCount = 10;

    private int valuesDistributionSinglePct = 50;
    private int valuesDistributionTenPct = 30;
    private int valuesDistributionThousandPct = 20;
    private Random random;
    private static String operationId = System.getenv("COMPUTERNAME");

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");

    private DatabaseClient databaseClient;
    private CSVPrinter csvPrinter;
    private DataFileWriter<MatchingAssetAvro> avroFileWriter;
    private String filename;
    private OutputStream outputStream;

    String projectId = "idev-01";

    public DBDataGenApp() {
        random = new Random(0L);
    }

    public DBDataGenApp(CommandLine cmd) {
        this();

        entityCount = Long.parseLong(cmd.getOptionValue(ENTITY_CNT_ARG, "" + entityCount));
        attributesCount = Integer.parseInt(cmd.getOptionValue(ENTITY_ATTR_CNT_ARG, "" + attributesCount));
        attributeValuesCount = Integer.parseInt(cmd.getOptionValue(ENTITY_ATTR_VALUES_CNT_ARG, "" + attributeValuesCount));

    }

    public void initData() {
        logger.info("Data generation started");

        long generatedCount = 0;

        Faker faker = new Faker(random);

        List<MatchingAsset> assets = new ArrayList<>();
        long lastReportTime = System.currentTimeMillis();

        ExecutorService executorService = Executors.newFixedThreadPool(nThreads);

        AtomicInteger busyCounter = new AtomicInteger(0);

        for (long i = 0; i < entityCount; i++) {
            Entity entity = generateEntity(i, faker);
            assets.addAll(convertEntity(entity));

            if (assets.size() > 100000) {
                generatedCount += assets.size();
                busyCounter.incrementAndGet();

                ArrayList<MatchingAsset> matchingAssets = new ArrayList<>(assets);
                executorService.submit(() -> {
                    try {
                        saveAssets(matchingAssets);
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    } finally {
                        busyCounter.decrementAndGet();
                    }
                });
                assets.clear();

                if (System.currentTimeMillis() - lastReportTime > 10_000) {
                    int pct = (int) ((double) i / entityCount * 100.0);

                    lastReportTime = System.currentTimeMillis();

                    logger.info("Data generation progress: {}, {}%", generatedCount, pct);
                }

                while (busyCounter.get() > nThreads * 2) {
                    try {
                        logger.info("Waiting for free workers");
                        Thread.sleep(5_000);
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            }
        }

        generatedCount += assets.size();
        saveAssets(assets);

        executorService.shutdown();
        try {
            executorService.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }

        logger.info("Data generation complete (assets: {})", generatedCount);
    }

    private void saveAssets(List<MatchingAsset> assets) {
        if (databaseClient != null) {

            List<Mutation> mutations = new ArrayList<>();
            for (MatchingAsset asset : assets) {
                mutations.add(
                        Mutation.newInsertOrUpdateBuilder("matching_assets")
                                .set("entity_id").to(asset.entityId)
                                .set("entity_type").to(asset.entityType)
                                .set("asset_name").to(asset.assetName)
                                .set("value").to(asset.value)
                                .set("entity_version").to(asset.entityVersion)
                                .set("updated_by").to(asset.updatedBy)
                                .set("update_timestamp").to(asset.updateTimestamp)
                                .set("update_operation_id").to(asset.updateOperationId)
                                .build());
            }

            List<MutationGroup> mutationGroups = ListUtils.partition(mutations, 1000).stream()
                    .map(MutationGroup::of)
                    .collect(Collectors.toList());

            try {
                ServerStream<BatchWriteResponse> responses = databaseClient.batchWriteAtLeastOnce(mutationGroups);
                for (BatchWriteResponse response : responses) {
                    if (response.getStatus().getCode() != Code.OK_VALUE) {
                        logger.warn(String.format(
                                "Mutation group indexes %s could not be applied with error code %s and "
                                        + "error message %s", response.getIndexesList(),
                                Code.forNumber(response.getStatus().getCode()), response.getStatus().getMessage())
                        );
                    }
                }
            } catch (Exception e) {
                throw e;
            }


        }

        if (csvPrinter != null) {
            for (MatchingAsset asset : assets) {
//                "entity_id", "entity_type", "asset_name", "value", "entity_version", "updated_by", "update_timestamp", "update_operation_id"
                try {
                    csvPrinter.printRecord(asset.entityId, asset.entityType, asset.assetName, asset.value, asset.entityVersion, asset.updatedBy, asset.updateTimestamp, asset.updateOperationId);
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
            try {
                csvPrinter.flush();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }

        if (avroFileWriter != null) {
            for (MatchingAsset asset : assets) {
                try {
                    avroFileWriter.append(convertToAvro(asset));
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
            try {
                avroFileWriter.flush();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }

        assets.clear();
    }

    private MatchingAssetAvro convertToAvro(MatchingAsset asset) {
        MatchingAssetAvro avro = new MatchingAssetAvro();

        avro.setEntityId(asset.entityId);
        avro.setEntityType(asset.entityType);
        avro.setEntityVersion(asset.entityVersion);
        avro.setAssetName(asset.assetName);
        avro.setValue(asset.value);
        avro.setUpdateTimestamp(asset.updateTimestamp);
        avro.setUpdatedBy(asset.updatedBy);
        avro.setUpdateOperationId(asset.updateOperationId);

        return avro;
    }

    private Entity generateEntity(long i, Faker faker) {
        Entity entity = new Entity();
        entity.id = String.format("%06X", i + 1);
        entity.type = "Person";
        entity.updatedBy = "DataGen";
        entity.version = "" + faker.number().numberBetween(1, 100);
        entity.updateTimestamp = System.currentTimeMillis();

        for (int j = 0; j < attributeValuesCount; j++) {
            entity.attributesValues.put("FirstName", faker.name().firstName());
            entity.attributesValues.put("LastName", faker.name().lastName());
            entity.attributesValues.put("birthdate", DATE_FORMAT.format(faker.date().birthday()));
            entity.attributesValues.put("Email_Email", faker.internet().emailAddress());
            entity.attributesValues.put("Phone_Number", faker.phoneNumber().cellPhone());
            entity.attributesValues.put("Address_Country", faker.address().country());
            entity.attributesValues.put("Address_StateProvince", faker.address().stateAbbr());
            entity.attributesValues.put("Address_City", faker.address().city());
            entity.attributesValues.put("Address_AddressLine1", faker.address().streetAddress());
            entity.attributesValues.put("Address_Zip", faker.address().zipCode().substring(0, 5));
        }

        return entity;
    }

    private List<MatchingAsset> convertEntity(Entity entity) {
        List<MatchingAsset> result = new ArrayList<>();

        for (Map.Entry<String, String> entry : entity.attributesValues.entries()) {
            MatchingAsset asset = new MatchingAsset();
            asset.entityId = entity.id;
            asset.entityType = entity.type;
            asset.entityVersion = entity.version;
            asset.assetName = entry.getKey();
            asset.value = entry.getValue();
            asset.updateTimestamp = entity.updateTimestamp;
            asset.updatedBy = entity.updatedBy;
            asset.updateOperationId = operationId;

            result.add(asset);
        }

        return result;
    }

    public void initDatabase() throws IOException {
        logger.info("DB creation");

        String instanceId = "rp139758";
        String databaseId = "merill";

        Spanner spanner = SpannerOptions.newBuilder()
                .setProjectId(projectId)
                .build()
                .getService();

        DatabaseId db = DatabaseId.of(projectId, instanceId, databaseId);
        databaseClient = spanner.getDatabaseClient(db);

        com.google.cloud.spanner.ResultSet resultSet = databaseClient
                .singleUse()
                .executeQuery(com.google.cloud.spanner.Statement.newBuilder("SELECT CURRENT_TIMESTAMP()").build());

        if (resultSet.next()) {
            logger.info("Spanner local time {}", resultSet.getTimestamp(0));
        } else {
            throw new RuntimeException("Error with Spanner connection");
        }
    }

    private void enableCsvOutput() throws IOException {
        csvPrinter = CSVFormat.DEFAULT.builder()
                .setHeader("entity_id", "entity_type", "asset_name", "value", "entity_version", "updated_by", "update_timestamp", "update_operation_id")
                .build()
                .print(new OutputStreamWriter(outputStream));

    }

    private void enableAvroOutput() throws IOException {
        DatumWriter<MatchingAssetAvro> userDatumWriter = new SpecificDatumWriter<>(MatchingAssetAvro.class);
        avroFileWriter = new DataFileWriter<>(userDatumWriter);
        avroFileWriter.create(new MatchingAssetAvro().getSchema(), outputStream);
    }

    private void setFilenameByOutput(String outputOptionValue) {
        if ("csv".equalsIgnoreCase(outputOptionValue)) {
            filename = "sample_data.csv";

        } else if ("avro".equalsIgnoreCase(outputOptionValue)) {
            filename = "sample_data.avro";

        }
    }

    private void setStorageType(String storageOptionValue, String storagePathOptionValue) throws FileNotFoundException {
        if ("gs".equalsIgnoreCase(storageOptionValue)) {
            Storage storage = StorageOptions.newBuilder().setProjectId(projectId).build().getService();

            String[] split = storagePathOptionValue.split("/", 1);
            // The name for the new bucket
            String bucketName = split[0];

            // Creates the new bucket
            Bucket bucket = storage.get(bucketName);

            com.google.cloud.storage.Blob blob = bucket.create(split[1] + "/" + System.currentTimeMillis() + "_" + filename, new byte[0]);
            outputStream = new BufferedOutputStream(Channels.newOutputStream(blob.writer()));

        } else {
            if (filename != null) {
                outputStream = new BufferedOutputStream(new FileOutputStream(filename, false));
            }

        }
    }

    private void close() {
        if (databaseClient != null) {
            databaseClient = null;
        }

        if (csvPrinter != null) {
            try {
                csvPrinter.close(true);
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
            csvPrinter = null;
        }

        if (avroFileWriter != null) {
            try {
                avroFileWriter.close();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
            avroFileWriter = null;
        }
    }

    public static void main(String[] args) throws ParseException, IOException {
        // create Options object
        Options options = new Options();

        options.addOption(OUTPUT_ARG);
        options.addOption(STORAGE_ARG);
        options.addOption(ENTITY_CNT_ARG);
        options.addOption(ENTITY_ATTR_CNT_ARG);
        options.addOption(ENTITY_ATTR_VALUES_CNT_ARG);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        DBDataGenApp main = new DBDataGenApp(cmd);

        String outputOptionValue = cmd.getOptionValue(OUTPUT_ARG, "spanner");
        main.setFilenameByOutput(outputOptionValue);

        String storageOptionValue = cmd.getOptionValue(STORAGE_ARG, "spanner");
        String storagePathOptionValue = cmd.getOptionValue(STORAGE_PATH_ARG, "imaltsev/test3");
        main.setStorageType(storageOptionValue, storagePathOptionValue);

        if ("spanner".equalsIgnoreCase(outputOptionValue)) {
            main.initDatabase();

        } else if ("csv".equalsIgnoreCase(outputOptionValue)) {
            main.enableCsvOutput();

        } else if ("avro".equalsIgnoreCase(outputOptionValue)) {
            main.enableAvroOutput();

        } else {
            throw new IllegalArgumentException("Unrecognized option: " + outputOptionValue);
        }

        main.initData();

        main.close();
    }
}
