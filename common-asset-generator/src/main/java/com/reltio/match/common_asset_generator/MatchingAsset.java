package com.reltio.match.common_asset_generator;

public class MatchingAsset {
    String entityId;
    String entityType;
    String entityVersion;
    String assetName;
    String value;
    Long updateTimestamp;
    String updatedBy;
    String updateOperationId;
}
