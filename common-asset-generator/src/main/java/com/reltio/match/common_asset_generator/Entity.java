package com.reltio.match.common_asset_generator;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

public class Entity {
    String id;
    String type;
    String version;
    MultiValuedMap<String, String> attributesValues = new ArrayListValuedHashMap<>();
    Long updateTimestamp;
    String updatedBy;
}
