Common Asset Data Generator
==

Setup
--

CLI to create a Spanner instance

`gcloud spanner instances create rp139758 --config=regional-us-east1 --instance-type=provisioned --processing-units=1000 --description=RP139758`

CLI to create a Spanner database

`gcloud spanner databases create merill --instance=rp139758  --database-dialect=GOOGLE_STANDARD_SQL`

SQL DDL to create a table

`
CREATE TABLE `matching_assets` (
    entity_id STRING(32), 
    entity_type STRING(256),
    asset_name STRING(1024), 
    value STRING(1024), 
    entity_version STRING(32), 
    updated_by STRING(32), 
    update_timestamp INT64, 
    update_operation_id STRING(128)
)
PRIMARY KEY
(asset_name, value, entity_id);
`

Run
--

Define env `GOOGLE_APPLICATION_CREDENTIALS` variable to point to Credentials Key File.

Run `com.reltio.match.common_asset_generator.DBDataGenApp`


Queries
--

`
SELECT count(*) FROM matching_assets;
`

Measure performance to get common asset extract

`
SELECT value, asset_name, entity_type, count(*) as `asset_count` FROM matching_assets GROUP by value, asset_name, entity_type having count(*) > 100;
`