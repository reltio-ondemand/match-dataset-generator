@SET JAVA_HOME=C:\Program Files\Java\jdk-14
@SET PICOCLI=%USERPROFILE%\.m2\repository\info\picocli\picocli\4.5.2\picocli-4.5.2.jar

"%JAVA_HOME%\bin\java.exe" -jar ".\target\match-dataset-generator-1.0-SNAPSHOT-jar-with-dependencies.jar" ^
 --entitiesTotal=75000 --outputFile="pot75K.txt" --appendCrosswalkAndPipe=true --entitiesBatchSize=1 ^
 --entityTypeName=IndividualA --entityIdOffset=0 ^
 --a1HighlyCollisionedTokenPrefix=highA1 --a1OverCollisionedTokenPrefix=overA1 ^
 --a1ValueSize=5 --attribute1Name=A1 --a1ValueDifferentValuesFactor=1.5 --a1DistributionSdFactor=2 ^
 --attribute2Name=A2 --a2ValueWordsCountFuzziness=5 --a2ValueWordsCountAverage=14 ^
 --a3HighlyCollisionedTokenPrefix=highA3 --a3OverCollisionedTokenPrefix=overA3 ^
 --a3ValueSize=5 --attribute3Name=A3 --a3ValueDifferentValuesFactor=1.5 --a3DistributionSdFactor=2 ^
 --attribute4Name=A4 --a4ValueWordsCountFuzziness=5 --a4ValueWordsCountAverage=14

"%JAVA_HOME%\bin\java.exe" -jar ".\target\match-dataset-generator-1.0-SNAPSHOT-jar-with-dependencies.jar" ^
 --entitiesTotal=25000 --outputFile="auto25K.txt" --appendCrosswalkAndPipe=true --entitiesBatchSize=1 ^
 --entityTypeName=IndividualB --entityIdOffset=75000 ^
 --a1HighlyCollisionedTokenPrefix=highB1 --a1OverCollisionedTokenPrefix=overB1 ^
 --a1ValueSize=5 --attribute1Name=B1 --a1ValueDifferentValuesFactor=1.5 --a1DistributionSdFactor=2 ^
 --attribute2Name=B2 --a2ValueWordsCountFuzziness=5 --a2ValueWordsCountAverage=14 ^
 --a3HighlyCollisionedTokenPrefix=highB3 --a3OverCollisionedTokenPrefix=overB3 ^
 --a3ValueSize=5 --attribute3Name=B3 --a3ValueDifferentValuesFactor=1.5 --a3DistributionSdFactor=2 ^
 --attribute4Name=B4 --a4ValueWordsCountFuzziness=5 --a4ValueWordsCountAverage=14

"%JAVA_HOME%\bin\java.exe" -classpath ".\target\classes;%PICOCLI%" ^
 com.reltio.match.dataset_generator.utils.MergeFilesUtility ^
 -in=pot75K.txt,auto25K.txt ^
 -out=Synthetic_dataset_suspect75K_auto25K_1.txt --shuffle=true

@RM pot75K.txt auto25K.txt
