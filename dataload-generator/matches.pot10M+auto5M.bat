@SET JAVA_HOME=C:\Program Files\Java\jdk-14
@SET PICOCLI=%USERPROFILE%\.m2\repository\info\picocli\picocli\4.5.2\picocli-4.5.2.jar

"%JAVA_HOME%\bin\java.exe" -jar ".\target\match-dataset-generator-1.0-SNAPSHOT-jar-with-dependencies.jar" ^
 --entitiesTotal=10000000 --outputFile="pot10M.txt" ^
 --a2ValueWordsCountAverage=14 --appendCrosswalkAndPipe=true --entitiesBatchSize=1 ^
 --a2ValueWordsCountFuzziness=5 --attribute1Name=A1 --attribute2Name=A2 --entityTypeName=IndividualA ^
 --a1ValueDifferentValuesFactor=2 --a1DistributionSdFactor=2 ^
 --entityIdOffset=0 --highlyCollisionedTokenPrefix=highA --overCollisionedTokenPrefix=overA

"%JAVA_HOME%\bin\java.exe" -jar ".\target\match-dataset-generator-1.0-SNAPSHOT-jar-with-dependencies.jar" ^
 --entitiesTotal=5000000 --outputFile="auto5M.txt" ^
 --a2ValueWordsCountAverage=14 --appendCrosswalkAndPipe=true --entitiesBatchSize=1 ^
 --a2ValueWordsCountFuzziness=5 --attribute1Name=B1 --attribute2Name=B2 --entityTypeName=IndividualB ^
 --a1ValueDifferentValuesFactor=2 --a1DistributionSdFactor=2 ^
 --entityIdOffset=10000000 --highlyCollisionedTokenPrefix=highB --overCollisionedTokenPrefix=overB

"%JAVA_HOME%\bin\java.exe" -classpath ".\target\classes;%PICOCLI%" ^
 com.reltio.match.dataset_generator.utils.MergeFilesUtility ^
 -in=pot10M.txt,auto5M.txt ^
 -out=matches.pot10M+auto5M.txt --shuffle=true

@RM pot10M.txt auto5M.txt
