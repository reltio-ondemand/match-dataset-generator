package com.reltio.match.dataset_generator;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Attribute {
    @JsonProperty
    private final String value;

    public Attribute(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
