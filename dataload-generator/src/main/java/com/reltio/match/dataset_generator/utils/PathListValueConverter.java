package com.reltio.match.dataset_generator.utils;

import picocli.CommandLine;

import java.nio.file.Path;
import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

public class PathListValueConverter implements CommandLine.ITypeConverter<List<Path>> {
    @Override
    public List<Path> convert(String value) {
        String[] paths = value.split(",");
        PathValueConverter pathValueConverter = new PathValueConverter();
        return stream(paths).map(pathValueConverter::convert).collect(toList());
    }
}
