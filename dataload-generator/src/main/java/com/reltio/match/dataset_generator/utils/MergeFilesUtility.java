package com.reltio.match.dataset_generator.utils;

import picocli.CommandLine;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "MergeFileUtility", mixinStandardHelpOptions = true, version = "1.0",
        description = "Merges files into one")
public class MergeFilesUtility implements Callable<Void> {

    @CommandLine.Option(names = {"-in", "--inputFiles"},
            description = "Input files",
            required = true,
            type = List.class,
            converter = PathListValueConverter.class)
    private List<Path> inputFiles;

    @CommandLine.Option(names = {"-out", "--outputFile"},
            description = "Output file",
            required = true,
            type = Path.class,
            converter = PathValueConverter.class)
    private Path outputFile;

    @CommandLine.Option(names = {"--shuffle"},
            description = "Flag to enable shuttling of the lines",
            defaultValue = "false",
            type = Boolean.class)
    private boolean shuffle;

    @CommandLine.Option(names = {"--shuffleRandomSeed"},
            description = "Random seed for shuffling",
            type = Long.class)
    private Long shuffleRandomSeed;

    @Override
    public Void call() throws Exception {
        List<String> allLines = new ArrayList<>();
        for (Path inputFile : inputFiles) {
            List<String> lines = Files.readAllLines(inputFile);
            allLines.addAll(lines);
        }
        if (shuffle) {
            Collections.shuffle(allLines, shuffleRandomSeed != null ? new Random(shuffleRandomSeed) : new Random());
        }
        Files.write(outputFile, allLines);
        return null;
    }

    public static void main(String[] args) {
        CommandLine commandLine = new CommandLine(new MergeFilesUtility());
        int exitCode = commandLine.execute(args);
        System.exit(exitCode);
    }
}
