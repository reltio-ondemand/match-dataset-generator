package com.reltio.match.dataset_generator.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import picocli.CommandLine;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

@CommandLine.Command(name = "FindCrosswalksForMatches", mixinStandardHelpOptions = true, version = "1.0",
        description = "Find crosswalks for entities having matches")
public class FindCrosswalksForMatches implements Callable<Void> {
    @CommandLine.Option(names = {"-in", "--inputFile"},
            description = "Input file",
            required = true,
            type = Path.class,
            converter = PathValueConverter.class)
    private Path inputFile;

    @CommandLine.Option(names = {"-out", "--outputFile"},
            description = "Output file",
            required = true,
            type = Path.class,
            converter = PathValueConverter.class)
    private Path outputFile;

    @Override
    public Void call() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        List<String> lines = Files.readAllLines(inputFile);
        Map<String, Set<String>> a1ValueToCrosswalks = new HashMap<>();
        Map<String, Set<String>> a3ValueToCrosswalks = new HashMap<>();
        Map<String, String> a1CrosswalkToValue = new HashMap<>();
        Map<String, String> a3CrosswalkToValue = new HashMap<>();
        for (String line : lines) {
            String[] entity = line.split("\\|");
            JsonNode source = mapper.readTree(entity[1]);
            JsonNode a1Attribute = source.get(0).get("attributes").get("A1");
            if (a1Attribute == null) {
                continue;
            }
            String a1Value = a1Attribute.get(0).get("value").asText();
            JsonNode a3Attribute = source.get(0).get("attributes").get("A3");
            String a3Value = a3Attribute.get(0).get("value").asText();
            String crosswalk = source.get(0).get("crosswalks").get(0).get("value").asText();
            a1ValueToCrosswalks.computeIfAbsent(a1Value, k -> new HashSet<>()).add(crosswalk);
            a3ValueToCrosswalks.computeIfAbsent(a3Value, k -> new HashSet<>()).add(crosswalk);
            a1CrosswalkToValue.put(crosswalk, a1Value);
            a3CrosswalkToValue.put(crosswalk, a3Value);
            if (a1ValueToCrosswalks.size() > 100000 && a3ValueToCrosswalks.size() > 100000) {
                break;
            }
        }
        Set<String> a1CrosswalksWithMatches = a1ValueToCrosswalks.entrySet().stream()
                .filter(e -> e.getValue().size() > 1)
                .limit(5000)
                .flatMap(e -> e.getValue().stream())
                .sorted()
                .limit(2000)
                .collect(toCollection(LinkedHashSet::new));
        Set<String> a3CrosswalksWithMatches = a3ValueToCrosswalks.entrySet().stream()
                .filter(e -> e.getValue().size() > 1)
                .limit(5000)
                .flatMap(e -> e.getValue().stream())
                .sorted()
                .limit(2000)
                .collect(toCollection(LinkedHashSet::new));
        Map<Pair<String, String>, Integer> depths = new HashMap<>();
        for (Map.Entry<String, Set<String>> e : a1ValueToCrosswalks.entrySet()) {
            String a1Value = e.getKey();
            Set<String> a1Crosswalks = e.getValue();
            for (String a1Crosswalk : a1Crosswalks) {
                String a3Value = a3CrosswalkToValue.get(a1Crosswalk);
                if (a3Value != null) {
                    Set<String> a3Crosswalks = a3ValueToCrosswalks.get(a3Value);
                    for (String a3Crosswalk : a3Crosswalks) {
                        if (!a1Crosswalk.equals(a3Crosswalk)) {
                            ImmutablePair<String, String> pair1 = ImmutablePair.of(a1Crosswalk, a3Crosswalk);
                            depths.putIfAbsent(pair1, 1);
                            ImmutablePair<String, String> pair2 = ImmutablePair.of(a3Crosswalk, a1Crosswalk);
                            depths.putIfAbsent(pair2, 1);
                        }
                    }
                }
            }
        }
        TreeSet<String> transitives = depths.keySet().stream()
                .map(Pair::getKey)
                .collect(toCollection(TreeSet::new));
        transitives.removeAll(a1CrosswalksWithMatches);
        transitives.removeAll(a3CrosswalksWithMatches);
        transitives = transitives.stream().limit(2000).collect(toCollection(TreeSet::new));

        TreeSet<String> finalSet = new TreeSet<>(Sets.union(Sets.union(a1CrosswalksWithMatches, a3CrosswalksWithMatches), transitives));
        finalSet = finalSet.stream().map(id -> id + ",Test").collect(toCollection(TreeSet::new));

        Files.write(outputFile, finalSet);

        return null;
    }

    public static void main(String[] args) {
        CommandLine commandLine = new CommandLine(new FindCrosswalksForMatches());
        int exitCode = commandLine.execute(args);
        System.exit(exitCode);
    }
}
