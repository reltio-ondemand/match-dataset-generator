package com.reltio.match.dataset_generator;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Crosswalk {
    @JsonProperty
    private final String value;
    @JsonProperty
    private final String type;

    public Crosswalk(String value, String type) {
        this.value = value;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }
}
