package com.reltio.match.dataset_generator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.reltio.match.dataset_generator.utils.PathValueConverter;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.stat.Frequency;
import picocli.CommandLine;

import java.io.BufferedWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.Supplier;
import java.util.function.ToLongFunction;
import java.util.stream.IntStream;

import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.*;
import static picocli.CommandLine.Command;
import static picocli.CommandLine.Option;

@Command(name = "matchesdatasetmanydoclocks", mixinStandardHelpOptions = true, version = "1.0",
        description = "Generates the dataset with many changes of the same documents")
public class MatchesDatasetManyDocLocksGenerator implements Callable<Void> {

    @Option(names = {"-et", "--entitiesTotal"},
            description = "Total entities in the dataset",
            required = true,
            type = Integer.class)
    private int entitiesTotal;

    @Option(names = {"-out", "--outputFile"},
            description = "Output file for the entities",
            required = true,
            type = Path.class,
            converter = PathValueConverter.class)
    private Path outputFile;

    @Option(names = {"--numberOfEntitiesToDuplicate"},
            description = "Number of entities to select and duplicate them",
            defaultValue = "100",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int numberOfEntitiesToDuplicate;

    @Option(names = {"--duplicateFactor"},
            description = "Number of duplicates for each entity",
            defaultValue = "100",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int duplicateFactor;

    @Option(names = {"--a1ValueSize"},
            description = "Length of A1 value",
            defaultValue = "5",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int a1ValueSize;

    @Option(names = {"--a1ValueRandomSeed"},
            description = "Random seed for A1 values",
            defaultValue = "111111",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int a1ValueRandomSeed;

    @Option(names = {"--a1ValueDifferentValuesFactor"},
            description = "The factor to define how many A1 values are possible",
            defaultValue = "4",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Double.class)
    private double a1ValueDifferentValuesFactor;

    @Option(names = {"--a1DistributionSdFactor"},
            description = "The factor of the A1 distribution to get the standard deviation " +
                    "from the entities count: total/a1DistributionSdFactor = sd",
            defaultValue = "5.0",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Double.class)
    private double a1DistributionSdFactor;

    @Option(names = {"--attribute1Name"},
            description = "Name of an first matching attribute",
            required = true,
            type = String.class)
    private String attribute1Name;

    @Option(names = {"--a1HighlyCollisionedTokensRatio"},
            description = "Part of entities having highly-collisioned tokens",
            defaultValue = "0.01",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Double.class)
    private double a1HighlyCollisionedTokensRatio;

    @Option(names = {"--a1OverCollisionedTokensRatio"},
            description = "Part of entities having over-collisioned tokens",
            defaultValue = "0.005",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Double.class)
    private double a1OverCollisionedTokensRatio;

    @Option(names = {"--a1HighlyCollisionedTokenPrefix"},
            description = "Prefix for the A1 attribute highly-collisioned tokens",
            defaultValue = "high",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = String.class)
    private String a1HighlyCollisionedTokenPrefix;

    @Option(names = {"--a1OverCollisionedTokenPrefix"},
            description = "Prefix for the A1 attribute over-collisioned tokens",
            defaultValue = "over",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = String.class)
    private String a1OverCollisionedTokenPrefix;

    @Option(names = {"--a2ValueSize"},
            description = "Length of A2 value",
            defaultValue = "5",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int a2ValueSize;

    @Option(names = {"--a2ValueRandomSeed"},
            description = "Random seed for A2 values",
            defaultValue = "222222",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int a2ValueRandomSeed;

    @Option(names = {"--a2ValueWordsCountAverage"},
            description = "Average count of words in an A2 value",
            defaultValue = "10",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int a2ValueWordsCount;

    @Option(names = {"--a2ValueWordsCountFuzziness"},
            description = "Allowed random shift (-/0/+) from average count of words in an A2 value",
            defaultValue = "1",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int a2ValueWordsCountFuzziness;

    @Option(names = {"--attribute2Name"},
            description = "Name of a second matching attribute",
            required = true,
            type = String.class)
    private String attribute2Name;

    @Option(names = {"--appendCrosswalkAndPipe"},
            description = "Flag to suppress crosswalk value and pipe on the left in the output",
            defaultValue = "true",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Boolean.class)
    private boolean appendCrosswalkAndPipe;

    @Option(names = {"--entitiesBatchSize"},
            description = "Size of the entities batch in the output line",
            defaultValue = "1",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int entitiesBatchSize;

    @Option(names = {"--entityTypeName"},
            description = "Name of an entity type",
            required = true,
            type = String.class)
    private String entityType;

    @Option(names = {"--entityIdOffset"},
            description = "Offset of the entity identifiers",
            defaultValue = "0",
            showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
            type = Integer.class)
    private int entityIdOffset;


    @Override
    public Void call() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        Supplier<String> a1ValueSupplier = getA1ValueSupplier(entitiesTotal, a1ValueSize, a1ValueRandomSeed, a1DistributionSdFactor, a1ValueDifferentValuesFactor);
        Supplier<String> a2ValueSupplier = getA2ValueSupplier(entitiesTotal, a2ValueSize, a2ValueRandomSeed, a2ValueWordsCount, a2ValueWordsCountFuzziness);
        Supplier<String> a1HighlyCollisionedValueSupplier = getHighlyCollisionedValueSupplier(
                entitiesTotal, a1HighlyCollisionedTokensRatio, a1ValueRandomSeed, a1HighlyCollisionedTokenPrefix);
        Supplier<String> a1OverCollisionedValueSupplier = getOverCollisionedValueSupplier(
                entitiesTotal, a1OverCollisionedTokensRatio, a1ValueRandomSeed, a1OverCollisionedTokenPrefix);

        Frequency a1Freq = new Frequency();
        Frequency a2Freq = new Frequency();
        List<Entity> entities = new ArrayList<>();
        for (int i = 0; i < entitiesTotal; i++) {
            int id = i + entityIdOffset;
            String entityId = StringUtils.leftPad(String.valueOf(id), 7, '0');
            LinkedHashMap<String, List<Attribute>> attributesMap = new LinkedHashMap<>();
            {
                List<Attribute> a1Values = new ArrayList<>();
                String a1Value = a1ValueSupplier.get();
                String a1HighlyCollisionedValue = a1HighlyCollisionedValueSupplier.get();
                String a1OverCollisionedValue = a1OverCollisionedValueSupplier.get();
                if (a1HighlyCollisionedValue != null) {
                    a1Values.add(new Attribute(a1HighlyCollisionedValue));
                } else if (a1OverCollisionedValue != null) {
                    a1Values.add(new Attribute(a1OverCollisionedValue));
                } else {
                    a1Values.add(new Attribute(a1Value));
                }
                a1Freq.addValue(a1Values.get(0).getValue());
                attributesMap.put(attribute1Name, a1Values);
            }
            {
                List<Attribute> a2Values = new ArrayList<>();
                a2Values.add(new Attribute(a2ValueSupplier.get()));
                attributesMap.put(attribute2Name, a2Values);
                a2Freq.addValue(a2Values.get(0).getValue());
            }
            List<Crosswalk> crosswalks = List.of(new Crosswalk(entityId, "configuration/sources/Test"));
            Entity entity = new Entity("configuration/entityTypes/" + entityType, attributesMap, crosswalks);
            entities.add(entity);
        }
        Set<String> allA1Values = new HashSet<>();
        a1Freq.valuesIterator().forEachRemaining(e -> allA1Values.add((String) e));
        System.out.println(attribute1Name + " different values: " + allA1Values.size());
        printMatchesRelatedInfo(a1Freq, attribute1Name);
        System.out.println();

        Set<String> allA2Values = new HashSet<>();
        a2Freq.valuesIterator().forEachRemaining(e -> allA2Values.add((String) e));
        System.out.println(attribute2Name + " different values: " + allA2Values.size());
        System.out.println();

        boolean attributeA1Yes = true;
        List<Entity> duplicates = new ArrayList<>();
        for (int i = 0; i < numberOfEntitiesToDuplicate; i++) {
            Entity entity = entities.get(i);
            for (int j = 0; j < duplicateFactor; j++) {
                LinkedHashMap<String, List<Attribute>> attributes = new LinkedHashMap<>(entity.getAttributes());
                if (!attributeA1Yes) {
                    attributes.remove(attribute1Name);
                }
                attributeA1Yes = !attributeA1Yes;
                List<Crosswalk> crosswalks = new ArrayList<>(entity.getCrosswalks());
                Entity newEntity = new Entity(
                        entity.getType(), attributes, crosswalks);
                duplicates.add(newEntity);
            }
        }
        List<Entity> finalEntities = new ArrayList<>(entities);
        finalEntities.addAll(duplicates);
        Collections.shuffle(finalEntities, new Random(123456789L));

        try (BufferedWriter writer = Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8)) {
            List<List<Entity>> partition = Lists.partition(finalEntities, entitiesBatchSize);
            for (List<Entity> entityList : partition) {
                String line = createOutputLine(appendCrosswalkAndPipe, objectMapper, entityList);
                writer.write(line);
                writer.newLine();
            }
        }
        return null;
    }

    private void printMatchesRelatedInfo(Frequency a1Freq, String attributeName) {
        List<Map.Entry<Comparable<?>, Long>> frequencies = new ArrayList<>();
        a1Freq.entrySetIterator().forEachRemaining(frequencies::add);
        frequencies.sort(comparingLong((ToLongFunction<Map.Entry<Comparable<?>, Long>>) Map.Entry::getValue).reversed());
        TreeMap<Long, Long> counts = frequencies.stream()
                .collect(groupingBy(Map.Entry::getValue, TreeMap::new, counting()));
        System.out.println(attributeName + " value frequencies: number of shares -> values count");
        String line1 = counts.entrySet().stream().limit(10).map(Object::toString).collect(joining(", "));
        System.out.println(line1);
        long entitiesWithHighlyCollisionedTokens = counts.entrySet().stream()
                .filter(e -> e.getKey() > 100 && e.getKey() < 300)
                .mapToLong(e -> e.getKey() * e.getValue())
                .sum();
        System.out.println("Entities with highly-collisioned tokens by " + attributeName + ": " + entitiesWithHighlyCollisionedTokens);
        long entitiesWithOverCollisionedTokens = counts.entrySet().stream()
                .filter(e -> e.getKey() >= 300)
                .mapToLong(e -> e.getKey() * e.getValue())
                .sum();
        System.out.println("Entities with over-collisioned tokens by " + attributeName + ": " + entitiesWithOverCollisionedTokens);
    }

    public static void main(String[] args) {
        CommandLine commandLine = new CommandLine(new MatchesDatasetManyDocLocksGenerator());
        int exitCode = commandLine.execute(args);
        System.exit(exitCode);
    }

    private static String createOutputLine(boolean appendCrosswalkAndPipe, ObjectMapper objectMapper, List<Entity> entityList) throws JsonProcessingException {
        String line;
        if (appendCrosswalkAndPipe) {
            line = entityList.get(0).getCrosswalks().get(0).getValue() + "|" + objectMapper.writeValueAsString(entityList);
        } else {
            line = objectMapper.writeValueAsString(entityList);
        }
        return line;
    }

    private static Supplier<String> getOverCollisionedValueSupplier(
            int entitiesTotal, double a1OverCollisionedTokensRatio, int a1ValueRandomSeed,
            String overCollisionedTokenPrefix) {
        List<String> values = IntStream.range(0, (int) (entitiesTotal * a1OverCollisionedTokensRatio / 400))
                .mapToObj(i -> {
                    String index = StringUtils.leftPad(String.valueOf(i), 5, '0');
                    return overCollisionedTokenPrefix + index;
                })
                .collect(toList());
        Random random = new Random(a1ValueRandomSeed);
        int bound = (int) (entitiesTotal * a1OverCollisionedTokensRatio);
        return () -> {
            int randomIndex = random.nextInt(entitiesTotal);
            if (randomIndex < bound && !values.isEmpty()) {
                return values.get(random.nextInt(values.size()));
            } else {
                return null;
            }
        };
    }

    private static Supplier<String> getHighlyCollisionedValueSupplier(
            int entitiesTotal, double a1HighlyCollisionedTokensRatio, int a1ValueRandomSeed,
            String highlyCollisionedTokenPrefix) {
        List<String> values = IntStream.range(0, (int) (entitiesTotal * a1HighlyCollisionedTokensRatio / 150))
                .mapToObj(i -> {
                    String index = StringUtils.leftPad(String.valueOf(i), 5, '0');
                    return highlyCollisionedTokenPrefix + index;
                })
                .collect(toList());
        Random random = new Random(a1ValueRandomSeed);
        int bound = (int) (entitiesTotal * a1HighlyCollisionedTokensRatio);
        return () -> {
            int randomIndex = random.nextInt(entitiesTotal);
            if (randomIndex < bound && !values.isEmpty()) {
                return values.get(random.nextInt(values.size()));
            } else {
                return null;
            }
        };
    }

    private static Supplier<String> getA2ValueSupplier(int entitiesTotal, int a2ValueSize,
                                                       int a2ValueRandomSeed, int a2ValueWordsCount,
                                                       int a2ValueWordsCountFuzziness) {
        Random random = new Random(a2ValueRandomSeed);
        List<String> a2Values = IntStream.range(0, entitiesTotal)
                .mapToObj(i -> {
                    int wordsCount = a2ValueWordsCount
                            + (random.nextInt(a2ValueWordsCountFuzziness * 2 + 1) - a2ValueWordsCountFuzziness);
                    List<String> words = IntStream.range(0, wordsCount)
                            .mapToObj(j -> RandomStringUtils.random(a2ValueSize, 0, 0, true, false, null, random))
                            .collect(toList());
                    return String.join(" ", words);
                })
                .collect(toList());
        Iterator<String> valueIterator = a2Values.iterator();
        return valueIterator::next;
    }

    private static Supplier<String> getA1ValueSupplier(int entitiesTotal, int a1ValueSize,
                                                       int a1ValueRandomSeed, double a1DistributionSdFactor,
                                                       double a1ValueDifferentValuesFactor) {
        Random random = new Random(a1ValueRandomSeed);
        int differentValuesTotal = (int) (entitiesTotal / a1ValueDifferentValuesFactor);
        List<String> a1Values = IntStream.range(0, differentValuesTotal)
                .mapToObj(i -> RandomStringUtils.random(a1ValueSize, 0, 0, true, false, null, random))
                .collect(toList());
        NormalDistribution normalDistribution = new NormalDistribution(
                new JDKRandomGenerator(a1ValueRandomSeed),
                differentValuesTotal / 2.0,
                differentValuesTotal / a1DistributionSdFactor);
        return () -> a1Values.get(getA1Index(differentValuesTotal, normalDistribution));
    }

    private static int getA1Index(int entitiesTotal, NormalDistribution normalDistribution) {
        int index = (int) normalDistribution.sample();
        if (index >= entitiesTotal
            || index < 0) {
            return getA1Index(entitiesTotal, normalDistribution);
        }
        return index;
    }

}
