package com.reltio.match.dataset_generator.utils;

import picocli.CommandLine;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathValueConverter implements CommandLine.ITypeConverter<Path> {
    @Override
    public Path convert(String s) {
        Path path = Paths.get(s);
        if (!path.toAbsolutePath().getParent().toFile().exists() && !path.toAbsolutePath().getParent().toFile().mkdirs()) {
            throw new IllegalArgumentException("Cannot ensure that parent directory for " + s + " exists");
        }
        return path;
    }
}
