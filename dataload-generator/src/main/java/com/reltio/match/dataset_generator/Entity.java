package com.reltio.match.dataset_generator;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.LinkedHashMap;
import java.util.List;

@JsonPropertyOrder({"type", "attributes", "crosswalks"})
public class Entity {
    @JsonProperty
    private final String type;

    @JsonProperty
    private final List<Crosswalk> crosswalks;

    @JsonProperty
    private final LinkedHashMap<String, List<Attribute>> attributes;

    public Entity(String type, LinkedHashMap<String, List<Attribute>> attributes, List<Crosswalk> crosswalks) {
        this.type = type;
        this.attributes = attributes;
        this.crosswalks = crosswalks;
    }

    public String getType() {
        return type;
    }

    public List<Crosswalk> getCrosswalks() {
        return crosswalks;
    }

    public LinkedHashMap<String, List<Attribute>> getAttributes() {
        return attributes;
    }
}
