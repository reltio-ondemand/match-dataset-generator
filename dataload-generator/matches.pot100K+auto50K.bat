@SET JAVA_HOME=C:\Program Files\Java\jdk-14
@SET PICOCLI=%USERPROFILE%\.m2\repository\info\picocli\picocli\4.5.2\picocli-4.5.2.jar

"%JAVA_HOME%\bin\java.exe" -jar ".\target\match-dataset-generator-1.0-SNAPSHOT-jar-with-dependencies.jar" ^
 --entitiesTotal=100000 --outputFile="pot100K.txt" ^
 --appendCrosswalkAndPipe=true --entitiesBatchSize=1 --entityTypeName=IndividualA ^
 --entityIdOffset=0 --highlyCollisionedTokenPrefix=highA --overCollisionedTokenPrefix=overA ^
 --attribute1Name=A1 --a1ValueDifferentValuesFactor=1.5 --a1DistributionSdFactor=2 ^
 --attribute2Name=A2 --a2ValueWordsCountFuzziness=5 --a2ValueWordsCountAverage=14 ^
 --attribute3Name=A3 --a3ValueDifferentValuesFactor=1.5 --a3DistributionSdFactor=2

"%JAVA_HOME%\bin\java.exe" -jar ".\target\match-dataset-generator-1.0-SNAPSHOT-jar-with-dependencies.jar" ^
 --entitiesTotal=50000 --outputFile="auto50K.txt" ^
 --entityTypeName=IndividualB --appendCrosswalkAndPipe=true --entitiesBatchSize=1 ^
 --entityIdOffset=2000000 --highlyCollisionedTokenPrefix=highB --overCollisionedTokenPrefix=overB ^
 --attribute1Name=B1 --a1ValueDifferentValuesFactor=1.5 --a1DistributionSdFactor=2 ^
 --attribute2Name=B2 --a2ValueWordsCountAverage=14 --a2ValueWordsCountFuzziness=5 ^
 --attribute3Name=B3 --a3ValueDifferentValuesFactor=1.5 --a3DistributionSdFactor=2

"%JAVA_HOME%\bin\java.exe" -classpath ".\target\classes;%PICOCLI%" ^
 com.reltio.match.dataset_generator.utils.MergeFilesUtility ^
 -in=pot100K.txt,auto50K.txt ^
 -out=matches.pot100K+auto50K.txt --shuffle=true

@RM pot100K.txt auto50K.txt
